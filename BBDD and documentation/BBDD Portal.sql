CREATE DATABASE IF NOT EXISTS `eduportal`;
USE `eduportal`;

-- SUBJECTS_GRADES --
CREATE TABLE `SUBJECTS_GRADES`
(
    `cod_user`    VARCHAR(10) NOT NULL,
    `cod_subject` VARCHAR(10) NOT NULL,
    `grade`       DOUBLE      NOT NULL,
    `comment`     TEXT        NULL,
    PRIMARY KEY (`cod_user`, `cod_subject`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUB_SUBJECTS_GRADES --
CREATE TABLE `SUB_SUBJECTS_GRADES`
(
    `cod_user`        VARCHAR(10) NOT NULL,
    `cod_sub_subject` VARCHAR(10) NOT NULL,
    `grade`           DOUBLE      NOT NULL,
    `comment`         TEXT        NULL,
    PRIMARY KEY (`cod_user`, `cod_sub_subject`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ELEMENTS --
CREATE TABLE `ELEMENTS`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`             VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_block`       VARCHAR(10)  NOT NULL,
    `cod_sub_block`   VARCHAR(10)  NULL,
    `name`            VARCHAR(255) NOT NULL,
    `type`            VARCHAR(30)  NOT NULL,
    `multiplier`      INT(11)      NULL,
    `position`        INT(11)      NOT NULL,
    `description`     TEXT         NULL,
    `url`             VARCHAR(255) NULL,
    `file_route`      VARCHAR(255) NULL,
    `file_name`       VARCHAR(255) NULL,
    `file_type`       VARCHAR(255) NULL,
    `activation_date` DATETIME(6)  NULL,
    `visible`         BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ROLES --
CREATE TABLE `ROLES`
(
    `id`   INT(11) AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(20) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUBJECTS_SETTINGS --
CREATE TABLE `SUBJECTS_SETTINGS`
(
    `id`          INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod_subject` VARCHAR(10)  NOT NULL,
    `property`    VARCHAR(30)  NOT NULL,
    `value`       VARCHAR(255) NOT NULL,
    UNIQUE (`cod_subject`, `property`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- FILES_DELIVERED --
CREATE TABLE `FILES_DELIVERED`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`             VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_user`        VARCHAR(10)  NOT NULL,
    `cod_delivery`    VARCHAR(10)  NOT NULL,
    `type`            VARCHAR(255) NOT NULL,
    `name`            VARCHAR(255) NOT NULL,
    `route`           VARCHAR(255) NOT NULL,
    `corrected_route` VARCHAR(255) NULL,
    `delivered_date`  DATETIME(6)  NOT NULL,
    `active`          BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ELEMENTS_GRADES --
CREATE TABLE `ELEMENTS_GRADES`
(
    `cod_user`    VARCHAR(10) NOT NULL,
    `cod_element` VARCHAR(10) NOT NULL,
    `grade`       DOUBLE      NOT NULL,
    `comment`     TEXT        NULL,
    PRIMARY KEY (`cod_user`, `cod_element`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SCHOOL_YEARS_MEMBERS --
CREATE TABLE `SCHOOL_YEARS_MEMBERS`
(
    `cod_school_year` VARCHAR(10) NOT NULL,
    `cod_user`        VARCHAR(10) NOT NULL,
    `register_date`   DATETIME(6) NOT NULL,
    PRIMARY KEY (`cod_school_year`, `cod_user`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUBJECTS_MEMBERS --
CREATE TABLE `SUBJECTS_MEMBERS`
(
    `cod_subject`     VARCHAR(10) NOT NULL,
    `cod_user`        VARCHAR(10) NOT NULL,
    `register_date`   DATETIME(6) NOT NULL,
    `permission_type` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`cod_subject`, `cod_user`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- FILE_TYPES --
CREATE TABLE `FILE_TYPES`
(
    `id`   INT(11) AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(10) NOT NULL UNIQUE KEY
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- DELIVERIES_SETTINGS --
CREATE TABLE `DELIVERIES_SETTINGS`
(
    `id`           INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod_delivery` VARCHAR(10)  NOT NULL,
    `property`     VARCHAR(255) NOT NULL,
    `value`        VARCHAR(255) NOT NULL,
    UNIQUE (`cod_delivery`, `property`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- USERS --
CREATE TABLE `USERS`
(
    `id`                     INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`                    VARCHAR(10)  NOT NULL UNIQUE KEY,
    `username`               VARCHAR(20)  NOT NULL UNIQUE KEY,
    `password`               VARCHAR(255) NOT NULL,
    `first_name`             VARCHAR(255) NOT NULL,
    `surname1`               VARCHAR(255) NOT NULL,
    `surname2`               VARCHAR(255) NULL,
    `email`                  VARCHAR(255) NULL,
    `photo_url`              VARCHAR(255) NULL,
    `city`                   VARCHAR(255) NULL,
    `phone`                  VARCHAR(255) NULL,
    `last_access`            DATETIME(6)  NULL,
    `first_access`           DATETIME(6)  NULL,
    `id_role`                INT(11)      NOT NULL,
    `temporal_recovery_code` VARCHAR(100) NULL UNIQUE KEY,
    `active`                 BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SETTINGS --
CREATE TABLE `SETTINGS`
(
    `id`       INT(11) AUTO_INCREMENT PRIMARY KEY,
    `property` VARCHAR(30)  NOT NULL UNIQUE KEY,
    `value`    VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- USERS_SETTINGS --
CREATE TABLE `USERS_SETTINGS`
(
    `id`       INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod_user` VARCHAR(10)  NOT NULL,
    `property` VARCHAR(30)  NOT NULL,
    `value`    VARCHAR(255) NOT NULL,
    UNIQUE (`cod_user`, `property`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SCHOOL_YEARS_SETTINGS --
CREATE TABLE `SCHOOL_YEARS_SETTINGS`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod_school_year` VARCHAR(10)  NOT NULL,
    `property`        VARCHAR(30)  NOT NULL,
    `value`           VARCHAR(255) NOT NULL,
    UNIQUE (`cod_school_year`, `property`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUBJECTS --
CREATE TABLE `SUBJECTS`
(
    `id`                    INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`                   VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_school_year`       VARCHAR(10)  NOT NULL,
    `name`                  VARCHAR(255) NOT NULL,
    `continuous_assessment` BIT(1)       NOT NULL,
    `visible`               BIT(1)       NOT NULL,
    `matriculation_key`     VARCHAR(255) NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUB_SUBJECTS --
CREATE TABLE `SUB_SUBJECTS`
(
    `id`             INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`            VARCHAR(10) NOT NULL UNIQUE KEY,
    `cod_subject`    VARCHAR(10) NOT NULL,
    `cod_evaluation` VARCHAR(10) NOT NULL,
    `visible`        BIT(1)      NOT NULL,
    UNIQUE (`cod_subject`, `cod_evaluation`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- TYPES --
CREATE TABLE `TYPES`
(
    `id`   INT(11) AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(30) NOT NULL UNIQUE KEY
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- PERMISSIONS_TYPES --
CREATE TABLE `PERMISSIONS_TYPES`
(
    `id`   INT(11) AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(30) NOT NULL UNIQUE KEY
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- BLOCKS --
CREATE TABLE `BLOCKS`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`             VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_sub_subject` VARCHAR(10)  NOT NULL,
    `name`            VARCHAR(255) NOT NULL,
    `percent`         INT(11)      NULL,
    `type`            VARCHAR(30)  NULL,
    `activation_date` DATETIME(6)  NULL,
    `visible`         BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- DELIVERIES --
CREATE TABLE `DELIVERIES`
(
    `id`            INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`           VARCHAR(10) NOT NULL UNIQUE KEY,
    `cod_element`   VARCHAR(10) NOT NULL,
    `delivery_date` DATETIME(6) NOT NULL,
    `close_date`    DATETIME(6) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUB_BLOCKS --
CREATE TABLE `SUB_BLOCKS`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`             VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_block`       VARCHAR(10)  NOT NULL,
    `name`            VARCHAR(255) NOT NULL,
    `percent`         INT(11)      NULL,
    `type`            VARCHAR(30)  NULL,
    `activation_date` DATETIME(6)  NULL,
    `visible`         BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- FILE_TYPES_DELIVERIES --
CREATE TABLE `FILE_TYPES_DELIVERIES`
(
    `cod_delivery` VARCHAR(10) NOT NULL,
    `file_type`    VARCHAR(10) NOT NULL,
    PRIMARY KEY (`cod_delivery`, `file_type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- BLOCKS_GRADES --
CREATE TABLE `BLOCKS_GRADES`
(
    `cod_user`  VARCHAR(10) NOT NULL,
    `cod_block` VARCHAR(10) NOT NULL,
    `grade`     DOUBLE      NOT NULL,
    `comment`   TEXT        NULL,
    PRIMARY KEY (`cod_user`, `cod_block`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- REQUIREMENTS --
CREATE TABLE `REQUIREMENTS`
(
    `id`          INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`         VARCHAR(10) NOT NULL UNIQUE KEY,
    `cod_element` VARCHAR(10) NOT NULL,
    `description` TEXT        NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- MESSAGES --
CREATE TABLE `MESSAGES`
(
    `id`                INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod_sender_user`   VARCHAR(10) NOT NULL,
    `cod_receiver_user` VARCHAR(10) NOT NULL,
    `shipping_date`     DATETIME(6) NOT NULL,
    `text`              TEXT        NOT NULL,
    `read`              BIT(1)      NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SCHOOL_YEARS --
CREATE TABLE `SCHOOL_YEARS`
(
    `id`                INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`               VARCHAR(10)  NOT NULL UNIQUE KEY,
    `name`              VARCHAR(255) NOT NULL,
    `visible`           BIT(1)       NOT NULL,
    `matriculation_key` VARCHAR(255) NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- EVALUATIONS --
CREATE TABLE `EVALUATIONS`
(
    `id`              INT(11) AUTO_INCREMENT PRIMARY KEY,
    `cod`             VARCHAR(10)  NOT NULL UNIQUE KEY,
    `cod_school_year` VARCHAR(10)  NOT NULL,
    `name`            VARCHAR(255) NOT NULL,
    `position`        INT(11)      NOT NULL,
    `last_ordinary`   BIT(1)       NOT NULL,
    `extra_ordinary`  BIT(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- SUB_BLOCKS_GRADES --
CREATE TABLE `SUB_BLOCKS_GRADES`
(
    `cod_user`      VARCHAR(10) NOT NULL,
    `cod_sub_block` VARCHAR(10) NOT NULL,
    `grade`         DOUBLE      NOT NULL,
    `comment`       TEXT        NULL,
    PRIMARY KEY (`cod_user`, `cod_sub_block`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- OAUTH2 TABLES --
create table oauth_client_details
(
    client_id               VARCHAR(256) PRIMARY KEY,
    resource_ids            VARCHAR(256),
    client_secret           VARCHAR(256),
    scope                   VARCHAR(256),
    authorized_grant_types  VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities             VARCHAR(256),
    access_token_validity   INTEGER,
    refresh_token_validity  INTEGER,
    additional_information  VARCHAR(4096),
    autoapprove             VARCHAR(256)
);

create table oauth_client_token
(
    token_id          VARCHAR(256),
    token             BLOB,
    authentication_id VARCHAR(256) PRIMARY KEY,
    user_name         VARCHAR(256),
    client_id         VARCHAR(256)
);

create table oauth_access_token
(
    token_id          VARCHAR(255),
    token             BLOB,
    authentication_id VARCHAR(255) PRIMARY KEY,
    user_name         VARCHAR(255),
    client_id         VARCHAR(255),
    authentication    BLOB,
    refresh_token     VARCHAR(255)
);

create table oauth_refresh_token
(
    token_id       VARCHAR(255),
    token          BLOB,
    authentication BLOB
);

create table oauth_code
(
    code           VARCHAR(256),
    authentication BLOB
);

create table oauth_approvals
(
    userId         VARCHAR(256),
    clientId       VARCHAR(256),
    scope          VARCHAR(256),
    status         VARCHAR(10),
    expiresAt      TIMESTAMP,
    lastModifiedAt TIMESTAMP
);

-- FOREIGN KEYS --
ALTER TABLE `USERS`
    ADD CONSTRAINT fk_user_role FOREIGN KEY (`id_role`) REFERENCES ROLES (`id`);

ALTER TABLE `MESSAGES`
    ADD CONSTRAINT fk_messageSender_user FOREIGN KEY (`cod_sender_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_messageReceiver_user FOREIGN KEY (`cod_receiver_user`) REFERENCES USERS (`cod`);

ALTER TABLE `USERS_SETTINGS`
    ADD CONSTRAINT fk_userSetting_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`);

ALTER TABLE `EVALUATIONS`
    ADD CONSTRAINT fk_evaluation_schoolYear FOREIGN KEY (`cod_school_year`) REFERENCES SCHOOL_YEARS (`cod`);

ALTER TABLE `SCHOOL_YEARS_SETTINGS`
    ADD CONSTRAINT fk_schoolYearSetting_schoolYear FOREIGN KEY (`cod_school_year`) REFERENCES SCHOOL_YEARS (`cod`);

ALTER TABLE `SCHOOL_YEARS_MEMBERS`
    ADD CONSTRAINT fk_schoolYearMember_schoolYear FOREIGN KEY (`cod_school_year`) REFERENCES SCHOOL_YEARS (`cod`),
    ADD CONSTRAINT fk_schoolYearMember_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`);

ALTER TABLE `SUBJECTS`
    ADD CONSTRAINT fk_subject_schoolYear FOREIGN KEY (`cod_school_year`) REFERENCES SCHOOL_YEARS (`cod`);

ALTER TABLE `SUB_SUBJECTS`
    ADD CONSTRAINT fk_subSubject_evaluation FOREIGN KEY (`cod_evaluation`) REFERENCES EVALUATIONS (`cod`),
    ADD CONSTRAINT fk_subSubject_subject FOREIGN KEY (`cod_subject`) REFERENCES SUBJECTS (`cod`);

ALTER TABLE `SUBJECTS_MEMBERS`
    ADD CONSTRAINT fk_subjectMember_subject FOREIGN KEY (`cod_subject`) REFERENCES SUBJECTS (`cod`),
    ADD CONSTRAINT fk_subjectMember_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_subjectMember_permissionType FOREIGN KEY (`permission_type`) REFERENCES PERMISSIONS_TYPES (`name`);

ALTER TABLE `SUBJECTS_SETTINGS`
    ADD CONSTRAINT fk_subjectSetting_subject FOREIGN KEY (`cod_subject`) REFERENCES SUBJECTS (`cod`);

ALTER TABLE `SUBJECTS_GRADES`
    ADD CONSTRAINT fk_subjectGrade_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_subjectGrade_subject FOREIGN KEY (`cod_subject`) REFERENCES SUBJECTS (`cod`);

ALTER TABLE `BLOCKS`
    ADD CONSTRAINT fk_block_subSubject FOREIGN KEY (`cod_sub_subject`) REFERENCES SUB_SUBJECTS (`cod`),
    ADD CONSTRAINT fk_block_type FOREIGN KEY (`type`) REFERENCES TYPES (`name`);

ALTER TABLE `BLOCKS_GRADES`
    ADD CONSTRAINT fk_blockGrade_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_blockGrade_block FOREIGN KEY (`cod_block`) REFERENCES BLOCKS (`cod`);

ALTER TABLE `SUB_BLOCKS`
    ADD CONSTRAINT fk_subBlock_block FOREIGN KEY (`cod_block`) REFERENCES BLOCKS (`cod`),
    ADD CONSTRAINT fk_subBlock_type FOREIGN KEY (`type`) REFERENCES TYPES (`name`);

ALTER TABLE `SUB_BLOCKS_GRADES`
    ADD CONSTRAINT fk_subBlockGrade_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_subBlockGrade_subBlock FOREIGN KEY (`cod_sub_block`) REFERENCES SUB_BLOCKS (`cod`);

ALTER TABLE `ELEMENTS`
    ADD CONSTRAINT fk_element_block FOREIGN KEY (`cod_block`) REFERENCES BLOCKS (`cod`),
    ADD CONSTRAINT fk_element_subBlock FOREIGN KEY (`cod_sub_block`) REFERENCES SUB_BLOCKS (`cod`),
    ADD CONSTRAINT fk_element_type FOREIGN KEY (`type`) REFERENCES TYPES (`name`);

ALTER TABLE `ELEMENTS_GRADES`
    ADD CONSTRAINT fk_elementGrade_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_elementGrade_element FOREIGN KEY (`cod_element`) REFERENCES ELEMENTS (`cod`);

ALTER TABLE `REQUIREMENTS`
    ADD CONSTRAINT fk_requirement_element FOREIGN KEY (`cod_element`) REFERENCES ELEMENTS (`cod`);

ALTER TABLE `DELIVERIES`
    ADD CONSTRAINT fk_delivery_element FOREIGN KEY (`cod_element`) REFERENCES ELEMENTS (`cod`);

ALTER TABLE `DELIVERIES_SETTINGS`
    ADD CONSTRAINT fk_deliverySetting_delivery FOREIGN KEY (`cod_delivery`) REFERENCES DELIVERIES (`cod`);

ALTER TABLE `FILE_TYPES_DELIVERIES`
    ADD CONSTRAINT fk_fileTypesDelivery_delivery FOREIGN KEY (`cod_delivery`) REFERENCES DELIVERIES (`cod`),
    ADD CONSTRAINT fk_fileTypesDelivery_fileType FOREIGN KEY (`file_type`) REFERENCES FILE_TYPES (`name`);

ALTER TABLE `FILES_DELIVERED`
    ADD CONSTRAINT fk_fileDelivered_user FOREIGN KEY (`cod_user`) REFERENCES USERS (`cod`),
    ADD CONSTRAINT fk_fileDelivered_delivery FOREIGN KEY (`cod_delivery`) REFERENCES DELIVERIES (`cod`);

INSERT INTO `ROLES`
VALUES (null, 'ADMIN'),
       (null, 'TEACHER'),
       (null, 'STUDENT');
/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal;

import com.educosystem.eduportal.service.impl.FileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.nio.file.FileAlreadyExistsException;
import java.util.TimeZone;

@SpringBootApplication
public class EduportalApplication implements CommandLineRunner {
    final FileService fileService;

    public EduportalApplication(FileService fileService) {
        this.fileService = fileService;
    }

    public static void main(String[] args) {
        SpringApplication.run(EduportalApplication.class, args);
    }

    /**
     * Set the time zone of the server
     */
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    /**
     * Start the file service.
     *
     * @param args of the function.
     * @throws Exception if the execution fails.
     */
    @Override
    public void run(String... args) throws Exception {
        try {
            fileService.init();
        } catch (FileAlreadyExistsException ignored) {
        }
    }
}

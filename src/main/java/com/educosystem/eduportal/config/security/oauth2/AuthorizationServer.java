/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.config.security.oauth2;

import com.educosystem.eduportal.service.impl.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
@RequiredArgsConstructor
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {
    private static final String WEB_CLIENT = "web";
    private static final String APP_CLIENT = "app";
    private static final int WEB_ACCESS_TOKEN_VALIDITY = 28800;
    private static final int WEB_REFRESH_TOKEN_VALIDITY = 36000;
    private static final int APP_ACCESS_TOKEN_VALIDITY = 345600;
    private static final int APP_REFRESH_TOKEN_VALIDITY = 604800;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final DataSource dataSource;
    private static final String IMPLICIT_GRANT_TYPE = "implicit";
    private static final String REFRESH_TOKEN_GRANT_TYPE = "refresh_token";

    /**
     * Configure what users can request an access token.
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();
    }

    /**
     * Configure the clients for oauth2
     *
     * @param clients to configure.
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        CustomJdbcClientDetailsServiceBuilder clientDetailsServiceBuilder =
                new CustomJdbcClientDetailsServiceBuilder();
        clientDetailsServiceBuilder
                .dataSource(dataSource)
                .withClient(WEB_CLIENT)
                .authorizedGrantTypes(IMPLICIT_GRANT_TYPE, REFRESH_TOKEN_GRANT_TYPE)
                .scopes("ALL", "openid")
                .resourceIds("oauth2-resource")
                .redirectUris("http://localhost:4200/")
                .autoApprove("ALL", "openid")
                .accessTokenValiditySeconds(WEB_ACCESS_TOKEN_VALIDITY)
                .refreshTokenValiditySeconds(WEB_REFRESH_TOKEN_VALIDITY)
                .and()
                .withClient(APP_CLIENT)
                .authorizedGrantTypes(IMPLICIT_GRANT_TYPE, REFRESH_TOKEN_GRANT_TYPE)
                .scopes("READ", "SEND_MESSAGES")
                .redirectUris("**")
                .resourceIds("oauth2-resource")
                .accessTokenValiditySeconds(APP_ACCESS_TOKEN_VALIDITY)
                .refreshTokenValiditySeconds(APP_REFRESH_TOKEN_VALIDITY)
                .and().build();
        clients.setBuilder(clientDetailsServiceBuilder);
    }

    /**
     * Configure the user info and the database token.
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .tokenStore(tokenStore());
    }

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }
}

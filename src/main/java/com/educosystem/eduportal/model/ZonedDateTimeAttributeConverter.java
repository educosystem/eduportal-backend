/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Converter(autoApply = true)
public class ZonedDateTimeAttributeConverter implements AttributeConverter<ZonedDateTime, Timestamp> {

    /**
     * Parse from zoneDateTime into a timeStamp.
     *
     * @param zonedDateTime to parse.
     * @return a timeStamp.
     */
    @Override
    public Timestamp convertToDatabaseColumn(ZonedDateTime zonedDateTime) {
        return zonedDateTime == null ? null : Timestamp.valueOf(zonedDateTime.toLocalDateTime());
    }

    /**
     * Parse from timeStamp into a zoneDateTime
     *
     * @param sqlTimestamp to parse
     * @return a zonedDateTime.
     */
    @Override
    public ZonedDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
        return sqlTimestamp == null ? null : ZonedDateTime.of(sqlTimestamp.toLocalDateTime(), ZoneId.of("UTC"));
    }
}

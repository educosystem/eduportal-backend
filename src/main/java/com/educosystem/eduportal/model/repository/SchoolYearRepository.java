/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.repository;

import com.educosystem.eduportal.model.entity.SchoolYear;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SchoolYearRepository extends JpaRepository<SchoolYear, Integer> {
    Optional<SchoolYear> findByCod(String cod);

    Page<SchoolYear> findAllByVisibleTrue(Pageable pageable);

    Page<SchoolYear> findByNameContainsIgnoreCase(String name, Pageable pageable);

    Page<SchoolYear> findByNameContainsIgnoreCaseAndVisibleTrue(String name, Pageable pageable);

    @Query("SELECT MAX(cod) FROM SchoolYear WHERE cod like concat(:year,'%')")
    String getMaxCod(String year);

}

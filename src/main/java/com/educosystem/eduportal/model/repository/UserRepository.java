/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.repository;

import com.educosystem.eduportal.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByCod(String cod);

    Optional<User> findByUsername(String name);

    Page<User> findByUsernameContainsIgnoreCase(String name, Pageable pageable);

    @Query("SELECT MAX(cod) FROM User WHERE cod like concat(:year,'%')")
    String getMaxCod(String year);

    @Query("SELECT COUNT(u) FROM User u WHERE u.role.name='ADMIN'")
    Integer countAdmin();
}

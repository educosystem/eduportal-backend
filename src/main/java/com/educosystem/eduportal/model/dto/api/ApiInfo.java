/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.dto.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;

@Getter
@JsonPropertyOrder({"name", "description", "license", "copyright", "versioncode", "version"})
public class ApiInfo {
    private final String NAME = "EduPortal Api";
    private final String DESCRIPTION = "Manage school years, evaluations, subjects, grades of students," +
            " communication and contents of subjects divided in elements and blocks";
    private final String LICENSE = "GPL V3.0 or above go to https://www.gnu.org/licenses/ for more information";
    private final String COPYRIGHT = "Cosme José Nieto Pérez, 2020";
    private final String VERSIONCODE = "Alpha v0.1";
    private final int VERSION = 0;

}

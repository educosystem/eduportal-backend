/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.dto.school_year;

import com.educosystem.eduportal.model.entity.SchoolYearSetting;
import com.educosystem.eduportal.util.views.SchoolYearViews;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;

@Getter
@Setter
@JsonRootName("schoolYear")
@Relation(collectionRelation = "schoolYears")
public class SchoolYearDto extends RepresentationModel<SchoolYearDto> {
    @JsonView(SchoolYearViews.List.class)
    private String cod;
    @JsonView(SchoolYearViews.List.class)
    private String name;
    @JsonView(SchoolYearViews.List.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean visible;
    @JsonView(SchoolYearViews.Detail.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String matriculationKey;
    @JsonView(SchoolYearViews.Detail.class)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SchoolYearSetting> settings;
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.dto.user;

import com.educosystem.eduportal.util.views.UserViews;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.server.core.Relation;

import java.time.ZonedDateTime;

@Getter
@Setter
@JsonRootName("user")
@Relation(collectionRelation = "users")
public class UserDto {
    @JsonView(UserViews.Public.class)
    private String cod;
    @JsonView(UserViews.Public.class)
    private String username;
    @JsonView(UserViews.Public.class)
    private String profileImage;
    @JsonView(UserViews.Public.class)
    private String firstName;
    @JsonView(UserViews.Public.class)
    private String surname1;
    @JsonView(UserViews.Public.class)
    private String surname2;
    @JsonView({UserViews.Teacher.class, UserViews.Self.class})
    private String email;
    @JsonView({UserViews.Teacher.class, UserViews.Self.class})
    private String city;
    @JsonView({UserViews.Teacher.class, UserViews.Self.class})
    private String phone;
    @JsonView({UserViews.Admin.class, UserViews.Self.class})
    private ZonedDateTime lastAccess;
    @JsonView({UserViews.Admin.class, UserViews.Self.class})
    private ZonedDateTime firstAccess;
    @JsonView(UserViews.Public.class)
    private String role;
    @JsonView(UserViews.Admin.class)
    private Boolean active;
}

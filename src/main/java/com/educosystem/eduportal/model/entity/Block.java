/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import com.educosystem.eduportal.util.json.CustomZonedDateTimeDeserializer;
import com.educosystem.eduportal.util.json.CustomZonedDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "BLOCKS")
public class Block implements Serializable {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, length = 10, unique = true)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_sub_subject", referencedColumnName = "cod", nullable = false)
    private SubSubject subSubject;
    @Column(nullable = false)
    private String name;
    private Integer percent;
    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "name")
    private Type type;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(name = "activation_date")
    private ZonedDateTime activationDate;
    @Column(nullable = false)
    private boolean visible;
    @OneToMany(mappedBy = "block", cascade = CascadeType.ALL)
    private List<SubBlock> subBlocks;
    @OneToMany(mappedBy = "block")
    private List<Element> elements;
    @OneToMany(mappedBy = "id.block")
    private List<BlockGrade> blockGrades;

    @Tolerate
    public Block() {
    }
}

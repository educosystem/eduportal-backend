/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import com.educosystem.eduportal.util.json.CustomZonedDateTimeDeserializer;
import com.educosystem.eduportal.util.json.CustomZonedDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "DELIVERIES")
public class Delivery implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 10)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_element", nullable = false, referencedColumnName = "cod")
    private Element element;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(name = "delivery_date", nullable = false)
    private ZonedDateTime deliveryDate;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(name = "close_date", nullable = false)
    private ZonedDateTime closeDate;
    @ManyToMany
    @JoinTable(name = "FILE_TYPES_DELIVERIES",
            joinColumns = @JoinColumn(name = "cod_delivery", referencedColumnName = "cod"),
            inverseJoinColumns = @JoinColumn(name = "file_type", referencedColumnName = "name"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"cod_delivery", "file_type"}))
    private List<FileType> fileTypesAccepted;
    @OneToMany(mappedBy = "delivery")
    private List<DeliverySetting> settings;
    @OneToMany(mappedBy = "delivery")
    private List<FileDelivered> files;

    @Tolerate
    public Delivery() {
    }
}

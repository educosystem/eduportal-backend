/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@SuppressWarnings("JpaDataSourceORMInspection")
@Embeddable
public class ElementUser implements Serializable {
    @ManyToOne
    @JoinColumn(name = "cod_user", nullable = false, referencedColumnName = "cod")
    private User user;
    @ManyToOne
    @JoinColumn(name = "cod_element", nullable = false, referencedColumnName = "cod")
    private Element element;

    public ElementUser() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElementUser that = (ElementUser) o;
        return user.getCod().equals(that.user.getCod()) &&
                element.getCod().equals(that.element.getCod());
    }

    @Override
    public int hashCode() {
        return Objects.hash(user.getCod(), element.getCod());
    }
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "SUB_SUBJECTS")
public class SubSubject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, length = 10, unique = true)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_subject", nullable = false, referencedColumnName = "cod")
    private Subject subject;
    @ManyToOne
    @JoinColumn(name = "cod_evaluation", nullable = false, referencedColumnName = "cod")
    private Evaluation evaluation;
    @Column(nullable = false)
    private boolean visible;
    @OneToMany(mappedBy = "subSubject", cascade = CascadeType.ALL)
    private List<Block> blocks;
    @OneToMany(mappedBy = "id.subSubject", cascade = CascadeType.ALL)
    private List<SubSubjectGrade> subSubjectGrades;

    @Tolerate
    public SubSubject() {
    }
}

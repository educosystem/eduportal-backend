/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Data
@Entity
@Table(name = "DELIVERIES_SETTINGS", uniqueConstraints = @UniqueConstraint(columnNames = {"property", "cod_delivery"}))
public class DeliverySetting implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "cod_delivery", referencedColumnName = "cod", nullable = false)
    private Delivery delivery;
    @Column(nullable = false, length = 30)
    private String property;
    @Column(nullable = false)
    private String value;

    @Tolerate
    public DeliverySetting() {
    }
}

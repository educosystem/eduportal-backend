/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 *//*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.educosystem.eduportal.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "SUBJECTS")
public class Subject implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, length = 10, unique = true)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_school_year", nullable = false, referencedColumnName = "cod")
    private SchoolYear schoolYear;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private boolean visible;
    @Column(name = "continuous_assessment", nullable = false)
    private boolean continuousAssessment;
    @Column(name = "matriculation_key")
    private String matriculationKey;
    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    private List<SubjectSetting> settings;
    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    private List<SubSubject> subSubjects;
    @OneToMany(mappedBy = "id.subject")
    private List<SubjectMember> members;
    @OneToMany(mappedBy = "id.subject")
    private List<SubjectGrade> subjectGrades;

    @Tolerate
    public Subject() {
    }
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "EVALUATIONS")
public class Evaluation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 10)
    private String cod;
    @JoinColumn(name = "cod_school_year", nullable = false, referencedColumnName = "cod")
    @ManyToOne
    private SchoolYear schoolYear;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer position;
    @Column(nullable = false, name = "last_ordinary")
    private boolean lastOrdinary;
    @Column(nullable = false, name = "extra_ordinary")
    private boolean extraOrdinary;
    @OneToMany(mappedBy = "evaluation")
    private List<SubSubject> subSubjects;

    @Tolerate
    public Evaluation() {
    }
}

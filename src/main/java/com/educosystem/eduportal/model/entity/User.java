/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import com.educosystem.eduportal.util.json.CustomZonedDateTimeDeserializer;
import com.educosystem.eduportal.util.json.CustomZonedDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "USERS")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 10)
    private String cod;
    @Column(nullable = false, unique = true, length = 20)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false, name = "first_name")
    private String firstName;
    @Column(nullable = false)
    private String surname1;
    private String surname2;
    private String email;
    @Column(name = "photo_url")
    private String photoUrl;
    private String city;
    private String phone;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(name = "first_access")
    private ZonedDateTime fistAccess;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(name = "last_access")
    private ZonedDateTime lastAccess;
    @Column(name = "temporal_recovery_code", unique = true, length = 100)
    private String temporalRecoveryCode;
    @OneToOne(optional = false)
    @JoinColumn(name = "id_role")
    private Role role;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<UserSetting> userSettings;
    @OneToMany(mappedBy = "receiverUser", cascade = CascadeType.ALL)
    private List<Message> messagesReceived;
    @OneToMany(mappedBy = "senderUser", cascade = CascadeType.ALL)
    private List<Message> messagesSent;
    @OneToMany(mappedBy = "id.user")
    private List<SubjectGrade> subjectGrades;
    @OneToMany(mappedBy = "id.user")
    private List<BlockGrade> blockGrades;
    @OneToMany(mappedBy = "id.user")
    private List<SubBlockGrade> subBlockGrades;
    @OneToMany(mappedBy = "id.user")
    private List<ElementGrade> elementGrades;
    @Column(nullable = false)
    private boolean active;

    @Tolerate
    public User() {
    }
}

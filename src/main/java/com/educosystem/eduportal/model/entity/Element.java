/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import com.educosystem.eduportal.util.json.CustomZonedDateTimeDeserializer;
import com.educosystem.eduportal.util.json.CustomZonedDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Data
@Entity
@Table(name = "ELEMENTS")
public class Element implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 10)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_block", referencedColumnName = "cod", nullable = false)
    private Block block;
    @ManyToOne
    @JoinColumn(name = "cod_sub_block", referencedColumnName = "cod")
    private SubBlock subBlock;
    @Column(nullable = false)
    private String name;
    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "name", nullable = false)
    private Type type;
    @Column(nullable = false)
    private Integer position;
    @Column(nullable = false)
    private Integer multiplier;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(nullable = false, name = "activation_date")
    private ZonedDateTime activationDate;
    private String url;
    @Column(columnDefinition = "TEXT")
    private String description;
    @Column(name = "file_route")
    private String fileRoute;
    @Column(name = "file_name")
    private String fileName;
    @ManyToOne
    @JoinColumn(name = "file_type", referencedColumnName = "name")
    private FileType fileType;
    @Column(nullable = false)
    private boolean visible;
    @OneToMany(mappedBy = "element")
    private List<Requirement> requirements;
    @OneToMany(mappedBy = "id.element")
    private List<ElementGrade> elementGrades;

    @Tolerate
    public Element() {
    }
}

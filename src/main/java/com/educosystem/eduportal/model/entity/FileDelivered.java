/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */
package com.educosystem.eduportal.model.entity;

import com.educosystem.eduportal.util.json.CustomZonedDateTimeDeserializer;
import com.educosystem.eduportal.util.json.CustomZonedDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Builder
@Data
@Entity
@Table(name = "FILES_DELIVERED")
public class FileDelivered implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true, length = 10)
    private String cod;
    @ManyToOne
    @JoinColumn(name = "cod_user", nullable = false, referencedColumnName = "cod")
    private User user;
    @ManyToOne
    @JoinColumn(name = "cod_delivery", nullable = false, referencedColumnName = "cod")
    private Delivery delivery;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String route;
    @Column(name = "corrected_route")
    private String correctedRoute;
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @Column(nullable = false, name = "delivered_date")
    private ZonedDateTime deliveredDate;
    @Column(nullable = false)
    private boolean active;

    @Tolerate
    public FileDelivered() {
    }
}

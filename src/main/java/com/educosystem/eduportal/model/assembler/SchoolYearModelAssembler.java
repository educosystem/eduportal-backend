/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.assembler;

import com.educosystem.eduportal.controller.SchoolYearRestController;
import com.educosystem.eduportal.model.dto.school_year.SchoolYearDto;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

@Component
public class SchoolYearModelAssembler
        implements RepresentationModelAssembler<SchoolYearDto, EntityModel<SchoolYearDto>> {
    private boolean adminView = false;

    /**
     * Provide the links for the different methods and controller available.
     *
     * @param entity to add links.
     * @return an entity model with all links.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public EntityModel<SchoolYearDto> toModel(SchoolYearDto entity) {
        EntityModel<SchoolYearDto> entityModel;

        Link self = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(SchoolYearRestController.class).get(entity.getCod(), null, null)
        ).withSelfRel().withType("GET");
        Link all = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                        .getList(null, null,
                                new PagedResourcesAssembler<>(null, null),
                                null)
        ).withRel("schoolYears").withType("GET");

        if (adminView) {
            Link create = null;
            try {
                create = WebMvcLinkBuilder.linkTo(
                        WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                                .create(null, null)
                ).withRel("create").withType("POST");
            } catch (NoSuchMethodException | MethodArgumentNotValidException e) {
                e.printStackTrace();
            }

            Link update = null;
            try {
                update = WebMvcLinkBuilder.linkTo(
                        WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                                .update(entity.getCod(), null, null, null)
                ).withRel("update").withType("PUT");
            } catch (NoSuchMethodException | MethodArgumentNotValidException e) {
                e.printStackTrace();
            }

            Link delete = WebMvcLinkBuilder.linkTo(
                    WebMvcLinkBuilder.methodOn(SchoolYearRestController.class).delete(entity.getCod(), null)
            ).withRel("delete").withType("DELETE");

            Link visible = WebMvcLinkBuilder.linkTo(
                    WebMvcLinkBuilder.methodOn(SchoolYearRestController.class).visible(entity.getCod(), null)
            ).withRel("visible").withType("PATCH");

            Link invisible = WebMvcLinkBuilder.linkTo(
                    WebMvcLinkBuilder.methodOn(SchoolYearRestController.class).invisible(entity.getCod(), null)
            ).withRel("invisible").withType("PATCH");
            entityModel = new EntityModel<>(entity, self, create, update, delete, visible, invisible, all);
        } else {
            entityModel = new EntityModel<>(entity, self, all);
        }
        return entityModel;
    }

    public void setAdminView(boolean adminView) {
        this.adminView = adminView;
    }
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.model.assembler;

import com.educosystem.eduportal.controller.RootRestController;
import com.educosystem.eduportal.controller.SchoolYearRestController;
import com.educosystem.eduportal.controller.UserRestController;
import com.educosystem.eduportal.model.dto.api.ApiInfo;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ApiInfoModelAssembler implements RepresentationModelAssembler<ApiInfo, EntityModel<ApiInfo>> {
    /**
     * Provide the links for the different methods and controller available.
     *
     * @param entity to add links.
     * @return an entity model with all links.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public EntityModel<ApiInfo> toModel(ApiInfo entity) {
        Link self = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(RootRestController.class).get()
        ).withSelfRel().withType("GET");

        Link schoolYears = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                        .getList(null, null,
                                new PagedResourcesAssembler<>(null, null), null)
        ).withRel("schoolYears").withType("GET");

        Link users = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(UserRestController.class)
                        .getList(null, null, new PagedResourcesAssembler<>(null, null))
        ).withRel("users").withType("GET");

        Link register = null;
        try {
            register = WebMvcLinkBuilder.linkTo(
                    WebMvcLinkBuilder.methodOn(UserRestController.class)
                            .create(null, null, null)
            ).withRel("register").withType("POST");
        } catch (Exception ignored) {
        }

        Link searchUsername = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(UserRestController.class)
                        .existsUsername(null)
        ).withRel("searchUsername").withType("GET");
        return new EntityModel<>(entity, self, schoolYears, users, register, searchUsername);
    }
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.controller;

import com.educosystem.eduportal.error.CustomizedResponseStatusException;
import com.educosystem.eduportal.model.assembler.SchoolYearModelAssembler;
import com.educosystem.eduportal.model.dto.school_year.SchoolYearDto;
import com.educosystem.eduportal.model.dto.school_year.SchoolYearPostDto;
import com.educosystem.eduportal.model.entity.SchoolYear;
import com.educosystem.eduportal.model.entity.User;
import com.educosystem.eduportal.service.impl.SchoolYearService;
import com.educosystem.eduportal.service.impl.UserService;
import com.educosystem.eduportal.util.Constant;
import com.educosystem.eduportal.util.views.SchoolYearViews;
import com.fasterxml.jackson.annotation.JsonView;
import org.modelmapper.ModelMapper;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/schoolYears")
public class SchoolYearRestController implements Constant {
    private final SchoolYearService schoolYearService;
    private final ModelMapper modelMapper;
    private final SchoolYearModelAssembler modelAssembler;
    private final UserService userService;
    private boolean adminView;

    public SchoolYearRestController(
            SchoolYearService schoolYearService,
            SchoolYearModelAssembler modelAssembler,
            UserService userService) {
        this.schoolYearService = schoolYearService;
        this.modelMapper = new ModelMapper();
        this.modelAssembler = modelAssembler;
        this.userService = userService;
    }

    /**
     * Transform the schoolYear into a dto.
     *
     * @param schoolYear to transform.
     * @return a schoolYearDto.
     */
    private SchoolYearDto convertToDto(SchoolYear schoolYear) {
        return modelMapper.map(schoolYear, SchoolYearDto.class);
    }

    /**
     * Transform the schoolYearDto into an entity.
     *
     * @param postDto to transform.
     * @param entity  to fuse.
     * @return a schoolYear entity.
     */
    private SchoolYear convertToEntity(SchoolYearPostDto postDto, SchoolYear entity) {
        modelMapper.map(postDto, entity);
        return entity;
    }

    /**
     * Get the user of authentication principal.
     *
     * @param principal user authenticated.
     * @return the user entity.
     */
    private com.educosystem.eduportal.model.entity.User findUser(
            org.springframework.security.core.userdetails.User principal
    ) {
        User user = null;
        if (principal != null)
            user = userService.findByUsername(principal.getUsername()).orElse(null);
        return user;
    }

    /**
     * @param name           to search.
     * @param pageable       for pagination results.
     * @param pagedAssembler to assemble the hateoas pagination.
     * @param principal      for get the actual user.
     * @return a response entity with the list of schoolYears.
     */
    @GetMapping
    public ResponseEntity<?> getList(
            @RequestParam(required = false) String name,
            @PageableDefault(sort = "cod") Pageable pageable,
            PagedResourcesAssembler<SchoolYearDto> pagedAssembler,
            @AuthenticationPrincipal org.springframework.security.core.userdetails.User principal
    ) {
        Page<SchoolYear> schoolYears;
        User user = findUser(principal);
        adminView = false;
        if (user != null)
            adminView = user.getRole().getName().equals(ADMIN);
        if (name == null)
            schoolYears = schoolYearService.list(pageable, !adminView);
        else
            schoolYears = schoolYearService.findByName(name, pageable, !adminView);
        Page<SchoolYearDto> dtoList = schoolYears.map(this::convertToDto);
        if (!adminView)
            for (SchoolYearDto s : dtoList) {
                s.setVisible(null);
            }
        modelAssembler.setAdminView(adminView);
        Link self = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                        .getList(null, null,
                                new PagedResourcesAssembler<>(null, null), null)
        ).withSelfRel().withType("GET");
        PagedModel<EntityModel<SchoolYearDto>> model = pagedAssembler.toModel(dtoList, modelAssembler, self);
        if (adminView) {
            Link create = null;
            try {
                //noinspection ConstantConditions
                create = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(SchoolYearRestController.class)
                        .create(null, null)
                ).withRel("create").withType("POST");
            } catch (NoSuchMethodException | MethodArgumentNotValidException e) {
                e.printStackTrace();
            }
            assert create != null;
            model.add(create);
        }
        return new ResponseEntity<Object>(
                model,
                HttpStatus.OK
        );
    }

    /**
     * @param cod       to search.
     * @param request   to throw exception if was not found.
     * @param principal for get the actual user.
     * @return a response entity with the schoolYear.
     */
    @JsonView(SchoolYearViews.Detail.class)
    @GetMapping("/{cod}")
    public ResponseEntity<?> get(@PathVariable String cod, HttpServletRequest request,
                                 @AuthenticationPrincipal org.springframework.security.core.userdetails.User principal) {
        SchoolYearDto schoolYear = convertToDto(findSchoolYear(cod, request));
        User user = findUser(principal);
        adminView = false;
        if (user != null)
            adminView = user.getRole().getName().equals(ADMIN);
        if (!adminView) {
            schoolYear.setVisible(null);
            schoolYear.setMatriculationKey(null);
            schoolYear.setSettings(new ArrayList<>());
        }
        modelAssembler.setAdminView(adminView);
        return ResponseEntity.ok(modelAssembler.toModel(schoolYear));
    }

    private SchoolYear findSchoolYear(String cod, HttpServletRequest request) {
        SchoolYear schoolYear = schoolYearService.get(cod).orElse(null);
        if (schoolYear == null)
            throw new CustomizedResponseStatusException(
                    HttpStatus.NOT_FOUND, "School year not found",
                    request.getRequestURI());
        return schoolYear;
    }

    /**
     * Create a new SchoolYear
     *
     * @param schoolYearDto    to create.
     * @param resultSchoolYear validation.
     * @return a response entity with the schoolYear created.
     * @throws NoSuchMethodException           if the method of throw was not found.
     * @throws MethodArgumentNotValidException if the schoolYearDto is not valid.
     */
    @JsonView(SchoolYearViews.Detail.class)
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@Valid @RequestBody SchoolYearPostDto schoolYearDto,
                                    BindingResult resultSchoolYear)
            throws NoSuchMethodException, MethodArgumentNotValidException {
        if (resultSchoolYear.hasErrors())
            throw new MethodArgumentNotValidException(
                    new MethodParameter(
                            this.getClass().getDeclaredMethod(
                                    "create",
                                    SchoolYearPostDto.class,
                                    BindingResult.class),
                            0),
                    resultSchoolYear);
        SchoolYear schoolYear = convertToEntity(schoolYearDto, new SchoolYear());
        schoolYear.setCod(schoolYearService.save(schoolYear));
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(schoolYear)), HttpStatus.CREATED);
    }

    /**
     * Update a existing schoolYear.
     *
     * @param cod              of the schoolYear
     * @param schoolYearDto    to create.
     * @param resultSchoolYear validation.
     * @param request          to throw exception.
     * @return a response entity with the schoolYear created.
     * @throws NoSuchMethodException           if the method of throw was not found.
     * @throws MethodArgumentNotValidException if the schoolYearDto is not valid.
     */
    @JsonView(SchoolYearViews.Detail.class)
    @PutMapping("{cod}")
    public ResponseEntity<?> update(@PathVariable String cod,
                                    @Valid @RequestBody SchoolYearPostDto schoolYearDto,
                                    BindingResult resultSchoolYear,
                                    HttpServletRequest request)
            throws MethodArgumentNotValidException, NoSuchMethodException {
        if (resultSchoolYear.hasErrors())
            throw new MethodArgumentNotValidException(
                    new MethodParameter(
                            this.getClass().getDeclaredMethod(
                                    "update",
                                    String.class,
                                    SchoolYearPostDto.class,
                                    BindingResult.class,
                                    HttpServletRequest.class),
                            0),
                    resultSchoolYear);
        SchoolYear schoolYear = findSchoolYear(cod, request);
        schoolYearService.update(convertToEntity(schoolYearDto, schoolYear));
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(schoolYear)), HttpStatus.OK);
    }

    /**
     * Make visible the schoolYear.
     *
     * @param cod     of the schoolYear to activate.
     * @param request to throw exceptions.
     * @return a response entity with the updated schoolYear.
     */
    @JsonView(SchoolYearViews.Detail.class)
    @PatchMapping("{cod}/visible")
    public ResponseEntity<?> visible(@PathVariable String cod, HttpServletRequest request) {
        SchoolYear schoolYear = findSchoolYear(cod, request);
        schoolYear.setVisible(true);
        schoolYearService.update(schoolYear);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(schoolYear)), HttpStatus.OK);
    }

    /**
     * Make invisible the schoolYear.
     *
     * @param cod     of the schoolYear to activate.
     * @param request to throw exceptions.
     * @return a response entity with the updated schoolYear.
     */
    @JsonView(SchoolYearViews.Detail.class)
    @PatchMapping("{cod}/invisible")
    public ResponseEntity<?> invisible(@PathVariable String cod, HttpServletRequest request) {
        SchoolYear schoolYear = findSchoolYear(cod, request);
        schoolYear.setVisible(false);
        schoolYearService.update(schoolYear);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(schoolYear)), HttpStatus.OK);
    }

    /**
     * Delete the schoolYear.
     *
     * @param cod     of the schoolYear to delete.
     * @param request to throw exceptions.
     * @return a httpStatus according with the result.
     */
    @DeleteMapping("{cod}")
    public ResponseEntity<?> delete(@PathVariable String cod, HttpServletRequest request) {
        SchoolYear schoolYear = findSchoolYear(cod, request);
        schoolYearService.delete(schoolYear);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}

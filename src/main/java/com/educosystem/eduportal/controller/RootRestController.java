/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.controller;

import com.educosystem.eduportal.model.assembler.ApiInfoModelAssembler;
import com.educosystem.eduportal.model.dto.api.ApiInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class RootRestController {
    private final ApiInfoModelAssembler modelAssembler;

    public RootRestController(ApiInfoModelAssembler modelAssembler) {
        this.modelAssembler = modelAssembler;
    }

    /**
     * @return a responseEntity with the information of the API.
     */
    @GetMapping
    @ResponseStatus
    public ResponseEntity<?> get() {
        return ResponseEntity.ok(modelAssembler.toModel(new ApiInfo()));
    }

    /**
     * Handle the login form.
     *
     * @param error of the login.
     * @param model to throw errors to the page.
     * @return a path of the page.
     */
    @GetMapping(path = "/login")
    public String login(@RequestParam(value = "error", required = false) String error, Model model) {
        if (error != null) {
            model.addAttribute("errors", "Username or password incorrect");
        }
        return "/login";
    }

    /**
     * Log out the actual user.
     *
     * @param request                  to retrieve information.
     * @param response                 for redirect.
     * @param post_logout_redirect_uri to redirect.
     */
    @GetMapping("/exit")
    public void exit(HttpServletRequest request, HttpServletResponse response,
                     @RequestParam(required = false) String post_logout_redirect_uri) {
        new SecurityContextLogoutHandler().logout(request, null, null);
        try {
            if (post_logout_redirect_uri != null)
                response.sendRedirect(post_logout_redirect_uri);
            else
                response.sendRedirect(request.getHeader("referer"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

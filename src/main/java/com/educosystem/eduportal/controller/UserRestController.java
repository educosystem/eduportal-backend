/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.controller;

import com.educosystem.eduportal.error.CustomizedResponseStatusException;
import com.educosystem.eduportal.model.assembler.UserModelAssembler;
import com.educosystem.eduportal.model.dto.school_year.SchoolYearPostDto;
import com.educosystem.eduportal.model.dto.user.UserCreateDto;
import com.educosystem.eduportal.model.dto.user.UserDto;
import com.educosystem.eduportal.model.dto.user.UserUpdateDto;
import com.educosystem.eduportal.model.entity.User;
import com.educosystem.eduportal.service.impl.FileService;
import com.educosystem.eduportal.service.impl.UserService;
import com.educosystem.eduportal.util.views.UserViews;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.commons.io.FileUtils;
import org.modelmapper.ModelMapper;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Base64;

@RestController
@RequestMapping("/users")
public class UserRestController {
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final UserModelAssembler modelAssembler;
    private final FileService fileService;
    private final PasswordEncoder passwordEncoder;

    public UserRestController(
            UserService userService,
            UserModelAssembler modelAssembler,
            FileService fileService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.modelMapper = new ModelMapper();
        this.modelAssembler = modelAssembler;
        this.fileService = fileService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Transform the user into a dto.
     *
     * @param user to transform.
     * @return a userDto
     */
    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setRole(user.getRole().getName());
        try {
            if (user.getPhotoUrl() != null) {
                byte[] fileContent = FileUtils.readFileToByteArray(fileService.load(user.getPhotoUrl()).getFile());
                userDto.setProfileImage(Base64.getEncoder().encodeToString(fileContent));
            }
        } catch (Exception ignored) {
        }
        return userDto;
    }

    /**
     * @param username       to search.
     * @param pageable       for pagination results.
     * @param pagedAssembler to assemble the hateoas pagination.
     * @return a response entity with the list of users.
     */
    @JsonView(UserViews.Admin.class)
    @GetMapping
    public ResponseEntity<?> getList(
            @RequestParam(required = false) String username,
            @PageableDefault(sort = "cod") Pageable pageable,
            PagedResourcesAssembler<UserDto> pagedAssembler
    ) {
        Page<User> users;
        if (username == null)
            users = userService.list(pageable);
        else
            users = userService.findByUsername(username, pageable);

        Page<UserDto> dtoList = users.map(this::convertToDto);
        Link self = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(UserRestController.class)
                        .getList(null, null,
                                new PagedResourcesAssembler<>(null, null))
        ).withSelfRel().withType("GET");
        Link create = null;
        try {
            //noinspection ConstantConditions
            create = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserRestController.class)
                    .create(null, null, null)
            ).withRel("create").withType("POST");
        } catch (NoSuchMethodException | MethodArgumentNotValidException e) {
            e.printStackTrace();
        }
        assert create != null;
        return new ResponseEntity<Object>(
                pagedAssembler.toModel(dtoList, modelAssembler, self).add(create),
                HttpStatus.OK
        );
    }

    /**
     * @param cod     of the user.
     * @param request to throw exception if was not found.
     * @return a response entity with the user found.
     */
    @JsonView(UserViews.Admin.class)
    @GetMapping("/{cod}")
    public ResponseEntity<?> get(@PathVariable String cod, HttpServletRequest request) {
        User user = findUser(cod, request);
        return ResponseEntity.ok(modelAssembler.toModel(convertToDto(user)));
    }

    /**
     * Search if one user have this name as username.
     *
     * @param name to search.
     * @return true if one user have this name, false otherwise.
     */
    @GetMapping("/search/{name}")
    public ResponseEntity<?> existsUsername(@PathVariable String name) {
        Boolean exist = userService.findByUsername(name).isPresent();
        Link self = WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(UserRestController.class).existsUsername(name)
        ).withSelfRel().withType("GET");
        return ResponseEntity.ok(new EntityModel<>(exist, self));
    }

    /**
     * Search one user.
     *
     * @param cod     to search.
     * @param request to throw exception if the user was not found.
     * @return the user found.
     */
    private User findUser(String cod, HttpServletRequest request) {
        User user = userService.get(cod).orElse(null);
        if (user == null)
            throw new CustomizedResponseStatusException(
                    HttpStatus.NOT_FOUND, "User not found",
                    request.getRequestURI());
        return user;
    }

    /**
     * Create a new user.
     *
     * @param userDto    to create.
     * @param resultUser validation.
     * @param request    to throw exception.
     * @return a response entity with the user created.
     * @throws NoSuchMethodException           if the method of throw was not found.
     * @throws MethodArgumentNotValidException if the userDto is not valid.
     */
    @JsonView(UserViews.Admin.class)
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@Valid @RequestBody UserCreateDto userDto,
                                    BindingResult resultUser,
                                    HttpServletRequest request)
            throws NoSuchMethodException, MethodArgumentNotValidException {
        if (userDto.getUsername() != null)
            if (userService.findByUsername(userDto.getUsername()).isPresent()) {
                resultUser.addError(new FieldError("user", "username",
                        "Username exists"));
            }
        if (userDto.getPassword1() != null && userDto.getPassword2() != null) {
            if (!userDto.getPassword2().equals(userDto.getPassword1()))
                resultUser.addError(new FieldError("user", "password2",
                        "Passwords don't match"));
        }
        if (resultUser.hasErrors())
            throw new MethodArgumentNotValidException(
                    new MethodParameter(
                            this.getClass().getDeclaredMethod(
                                    "create",
                                    UserCreateDto.class,
                                    BindingResult.class,
                                    HttpServletRequest.class),
                            0),
                    resultUser);
        User user = User.builder().cod(userService.getNewUserCod()).build();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        modelMapper.map(userDto, user);
        user.setPassword(passwordEncoder.encode(userDto.getPassword1()));
        user.setPhotoUrl(saveFile(userDto.getProfileImage(), user.getCod(), request));
        userService.save(user);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(user)), HttpStatus.CREATED);
    }

    /**
     * Save the photo profile file.
     *
     * @param profileImage base64 file.
     * @param cod          of the user.
     * @param request      to throw exception.
     * @return name of the file.
     */
    private String saveFile(String profileImage, String cod, HttpServletRequest request) {
        if (profileImage != null) {
            try {
                byte[] decodedBytes = Base64.getDecoder().decode(profileImage);
                fileService.copy(decodedBytes,
                        "profile_" + cod);
                return "profile_" + cod;
            } catch (Exception e) {
                throw new CustomizedResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR,
                        "Photo cannot be uploaded",
                        request.getRequestURI());
            }
        }
        return null;
    }

    /**
     * Update one user.
     *
     * @param cod           of user.
     * @param userUpdateDto to update.
     * @param resultUser    validation.
     * @param request       to throw exceptions.
     * @return a response entity with the updated user.
     * @throws NoSuchMethodException           if the method of throw was not found.
     * @throws MethodArgumentNotValidException if the userDto is not valid.
     */
    @PutMapping("{cod}")
    public ResponseEntity<?> update(@PathVariable String cod,
                                    @Valid @RequestBody UserUpdateDto userUpdateDto,
                                    BindingResult resultUser,
                                    HttpServletRequest request)
            throws MethodArgumentNotValidException, NoSuchMethodException {
        if (resultUser.hasErrors())
            throw new MethodArgumentNotValidException(
                    new MethodParameter(
                            this.getClass().getDeclaredMethod(
                                    "update",
                                    String.class,
                                    SchoolYearPostDto.class,
                                    BindingResult.class,
                                    HttpServletRequest.class),
                            0),
                    resultUser);
        User user = findUser(cod, request);
        modelMapper.map(userUpdateDto, user);
        user.setPhotoUrl(saveFile(userUpdateDto.getProfileImage(), user.getCod(), request));
        userService.update(user);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(user)), HttpStatus.OK);
    }

    /**
     * Activate the user.
     *
     * @param cod     of the user to activate.
     * @param request to throw exceptions.
     * @return a response entity with the updated user.
     */
    @PatchMapping("{cod}/active")
    public ResponseEntity<?> active(@PathVariable String cod, HttpServletRequest request) {
        User user = findUser(cod, request);
        user.setActive(true);
        userService.update(user);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(user)), HttpStatus.OK);
    }

    /**
     * Inactivate the user.
     *
     * @param cod     of the user to activate.
     * @param request to throw exceptions.
     * @return a response entity with the updated user.
     */
    @PatchMapping("{cod}/inactive")
    public ResponseEntity<?> inactive(@PathVariable String cod, HttpServletRequest request) {
        User user = findUser(cod, request);
        user.setActive(false);
        userService.update(user);
        return new ResponseEntity<Object>(modelAssembler.toModel(convertToDto(user)), HttpStatus.OK);
    }

    /**
     * Delete the user.
     *
     * @param cod     of the user to delete.
     * @param request to throw exceptions.
     * @return a httpStatus according with the result.
     */
    @DeleteMapping("{cod}")
    public ResponseEntity<?> delete(@PathVariable String cod, HttpServletRequest request) {
        User user = findUser(cod, request);
        if (user.getPhotoUrl() != null) {
            fileService.delete(user.getPhotoUrl());
        }
        userService.delete(user);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}

package com.educosystem.eduportal.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IFileService {

    Resource load(String filename) throws MalformedURLException;

    void copy(MultipartFile file, String name) throws IOException;

    void copy(byte[] contentFile, String name) throws IOException;

    boolean delete(String filename);

    void deleteAll();

    void init() throws IOException;
}

package com.educosystem.eduportal.service.impl;

import com.educosystem.eduportal.service.IFileService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileService implements IFileService {
    private static String UPLOADS_FOLDER;
    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Load the file on memory.
     *
     * @param filename to search.
     * @return a resource.
     * @throws MalformedURLException if the file was not found.
     */
    @Override
    public Resource load(String filename) throws MalformedURLException {
        Path pathPhoto = getPath(filename);
        log.info("pathPhoto: " + pathPhoto);
        Resource resource = new UrlResource(pathPhoto.toUri());
        if (!resource.exists() || !resource.isReadable()) {
            throw new RuntimeException("Error: The image cannot be loaded: " + pathPhoto.toString());
        }
        return resource;
    }

    /**
     * Copy and save a multipart file.
     *
     * @param file stream.
     * @param name of the file.
     * @throws IOException if the file cannot be saved.
     */
    @Override
    public void copy(MultipartFile file, String name) throws IOException {
        Path rootPath = getPath(name);
        log.info("rootPath: " + rootPath);
        Files.copy(file.getInputStream(), rootPath, StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Copy and save a byte file.
     *
     * @param contentFile to save.
     * @param name        of the file.
     * @throws IOException if the file cannot be saved.
     */
    @Override
    public void copy(byte[] contentFile, String name) throws IOException {
        Path rootPath = getPath(name);
        log.info("rootPath: " + rootPath);
        FileUtils.writeByteArrayToFile(new File(rootPath.toUri()), contentFile);
    }

    /**
     * Delete the file.
     *
     * @param filename to delete.
     * @return if the file was deleted.
     */
    @Override
    public boolean delete(String filename) {
        Path rootPath = getPath(filename);
        File file = rootPath.toFile();
        if (file.exists() && file.canRead()) {
            return file.delete();
        }
        return false;
    }

    /**
     * Get a path of a file.
     *
     * @param filename to search
     * @return the path of file.
     */
    public Path getPath(String filename) {
        return Paths.get(UPLOADS_FOLDER).resolve(filename).toAbsolutePath();
    }

    /**
     * Delete all files.
     */
    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_FOLDER).toFile());
    }

    /**
     * Initialize file service, creating the directory.
     *
     * @throws IOException if the directory already exists.
     */
    @Override
    public void init() throws IOException {
        UPLOADS_FOLDER = System.getProperty("user.home") + "/uploads_eduportal";
        Files.createDirectory(Paths.get(UPLOADS_FOLDER));
    }
}

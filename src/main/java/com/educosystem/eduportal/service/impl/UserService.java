/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.service.impl;

import com.educosystem.eduportal.model.entity.User;
import com.educosystem.eduportal.model.repository.RoleRepository;
import com.educosystem.eduportal.model.repository.UserRepository;
import com.educosystem.eduportal.service.IUserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;
import java.util.TimeZone;

@Service
public class UserService implements IUserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    /**
     * Save the user.
     *
     * @param item to save.
     * @return user cod.
     */
    @Override
    public String save(User item) {
        if (item.getRole() == null)
            if (userRepository.countAdmin() == 0) {
                item.setRole(roleRepository.findByName("ADMIN"));
            } else {
                item.setRole(roleRepository.findByName("STUDENT"));
            }
        userRepository.save(item);
        return item.getCod();
    }

    /**
     * Update the user.
     *
     * @param item to update.
     */
    @Override
    public void update(User item) {
        userRepository.save(item);
    }

    /**
     * Delete the user.
     *
     * @param item to delete.
     */
    @Override
    public void delete(User item) {
        userRepository.delete(item);
    }

    /**
     * List the users.
     *
     * @param pageable for pagination.
     * @return a page list of users.
     */
    @Override
    public Page<User> list(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    /**
     * Search user by username.
     *
     * @param name     to contain.
     * @param pageable for pagination.
     * @return a page list of users.
     */
    @Override
    public Page<User> findByUsername(String name, Pageable pageable) {
        return userRepository.findByUsernameContainsIgnoreCase(name, pageable);
    }

    /**
     * Search a user with the exact username.
     *
     * @param name to search.
     * @return the user found.
     */
    @Override
    public Optional<User> findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    /**
     * Get the user by cod.
     *
     * @param cod of user.
     * @return the user found.
     */
    @Override
    public Optional<User> get(String cod) {
        return userRepository.findByCod(cod);
    }

    /**
     * @return new valid cod for user.
     */
    @Override
    public String getNewUserCod() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        String cod = userRepository.getMaxCod(String.valueOf(calendar.get(Calendar.YEAR)).substring(1));
        String newCod = String.valueOf(calendar.get(Calendar.YEAR)).substring(1);
        if (cod != null) {
            int number = Integer.parseInt(cod.substring(3)) + 1;
            if (number > 0 && number < 10)
                newCod = newCod + "000000" + number;
            else if (number >= 10 && number < 100)
                newCod = newCod + "00000" + number;
            else if (number >= 100 && number < 1000)
                newCod = newCod + "0000" + number;
            else if (number >= 1000 && number < 10000)
                newCod = newCod + "000" + number;
            else if (number >= 10000 && number < 100000)
                newCod = newCod + "00" + number;
            else if (number >= 100000 && number < 1000000)
                newCod = newCod + "0" + number;
            else
                newCod = newCod + number;
        } else
            newCod = newCod + "0000001";
        return newCod;
    }
}

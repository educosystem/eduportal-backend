/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.service.impl;

import com.educosystem.eduportal.model.entity.SchoolYear;
import com.educosystem.eduportal.model.repository.SchoolYearRepository;
import com.educosystem.eduportal.service.ISchoolYearService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;
import java.util.TimeZone;

@Service
public class SchoolYearService implements ISchoolYearService {
    private final SchoolYearRepository schoolYearRepository;

    public SchoolYearService(SchoolYearRepository schoolYearRepository) {
        this.schoolYearRepository = schoolYearRepository;
    }

    /**
     * Save the schoolYear.
     *
     * @param item to save.
     * @return schoolYear cod.
     */
    @Override
    public String save(SchoolYear item) {
        item.setCod(getNewSchoolYearCod());
        schoolYearRepository.save(item);
        return item.getCod();
    }

    /**
     * Update the schoolYear.
     *
     * @param item to update.
     */
    @Override
    public void update(SchoolYear item) {
        schoolYearRepository.save(item);
    }

    /**
     * Delete the schoolYear.
     *
     * @param item to delete.
     */
    @Override
    public void delete(SchoolYear item) {
        schoolYearRepository.delete(item);
    }

    /**
     * List the schoolYears.
     *
     * @param pageable for pagination.
     * @return a page list of schoolYears.
     */
    @Override
    public Page<SchoolYear> list(Pageable pageable) {
        return list(pageable, false);
    }

    /**
     * List the schoolYears.
     *
     * @param pageable    for pagination.
     * @param visibleOnly to show visible items only.
     * @return a page list of schoolYears.
     */
    @Override
    public Page<SchoolYear> list(Pageable pageable, boolean visibleOnly) {
        Page<SchoolYear> schoolYears;
        if (visibleOnly)
            schoolYears = schoolYearRepository.findAllByVisibleTrue(pageable);
        else
            schoolYears = schoolYearRepository.findAll(pageable);
        return schoolYears;
    }

    /**
     * Search schoolYear by name.
     *
     * @param name        to contain.
     * @param pageable    for pagination.
     * @param visibleOnly to show visible items only.
     * @return a page list of schoolYears.
     */
    @Override
    public Page<SchoolYear> findByName(String name, Pageable pageable, boolean visibleOnly) {
        Page<SchoolYear> schoolYears;
        if (visibleOnly)
            schoolYears = schoolYearRepository.findByNameContainsIgnoreCaseAndVisibleTrue(name, pageable);
        else
            schoolYears = schoolYearRepository.findByNameContainsIgnoreCase(name, pageable);
        return schoolYears;
    }

    /**
     * Get the schoolYear by cod.
     *
     * @param cod of schoolYear.
     * @return the schoolYear found.
     */
    @Override
    public Optional<SchoolYear> get(String cod) {
        return schoolYearRepository.findByCod(cod);
    }

    /**
     * @return new valid cod for schoolYear.
     */
    private String getNewSchoolYearCod() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        String cod = schoolYearRepository.getMaxCod(String.valueOf(calendar.get(Calendar.YEAR)).substring(1));
        String newCod = String.valueOf(calendar.get(Calendar.YEAR)).substring(1);
        if (cod != null) {
            int number = Integer.parseInt(cod.substring(3)) + 1;
            if (number > 0 && number < 10)
                newCod = newCod + "00" + number;
            else if (number >= 10 && number < 100)
                newCod = newCod + "0" + number;
            else
                newCod = newCod + number;
        } else
            newCod = newCod + "001";
        return newCod;
    }
}

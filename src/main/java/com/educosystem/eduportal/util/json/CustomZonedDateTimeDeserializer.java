/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.util.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

public class CustomZonedDateTimeDeserializer extends StdDeserializer<ZonedDateTime> {

    public CustomZonedDateTimeDeserializer() {
        this(null);
    }

    protected CustomZonedDateTimeDeserializer(Class<?> vc) {
        super(vc);
    }

    /**
     * Transform the received date into a UTC zoneDateTime.
     *
     * @param jsonParser             for deserialize.
     * @param deserializationContext for deserialize.
     * @return a zonedDateTime parsed.
     * @throws IOException            if fail to deserialize.
     * @throws DateTimeParseException if fails  to parse.
     */
    @Override
    public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, DateTimeParseException {
        ZonedDateTime receivedTime = ZonedDateTime.parse(jsonParser.getText());
        return receivedTime.withZoneSameInstant(ZoneId.of("UTC"));
    }
}

/*
 * This file is part of EduPortal.
 *
 *     EduPortal is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     EduPortal is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with EduPortal.  If not, see <https://www.gnu.org/licenses/>.
 *
 *     Copyright Cosme José Nieto Pérez, 2020
 */

package com.educosystem.eduportal.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.ZonedDateTime;

public class CustomZonedDateTimeSerializer extends StdSerializer<ZonedDateTime> {

    public CustomZonedDateTimeSerializer() {
        this(null);
    }

    public CustomZonedDateTimeSerializer(Class<ZonedDateTime> t) {
        super(t);
    }

    /**
     * Serialize the zoneDateTime.
     *
     * @param zonedDateTime      to serialize.
     * @param jsonGenerator      for serialize.
     * @param serializerProvider for serialize.
     * @throws IOException if fails to serialize.
     */
    @Override
    public void serialize(ZonedDateTime zonedDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeString(zonedDateTime.toOffsetDateTime().toString());
    }
}

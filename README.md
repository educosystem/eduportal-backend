# Instalación

Cree una base de datos mysql llamada `eduportal`, un usuario `eduportal` con la
contraseña `education`. Puede crear las tablas a partir del fichero `BBDD Portal`
que se encuentra dentro de la carpeta `BBDD`.  
Una vez echo esto ya puede lanzar el servicio.  

### Cosas a tener en cuenta

Este proyecto tiene recursos protegidos con oauth2.0 para poder acceder a ellos
primero tiene que crear un usuario mediante postman y este sera por defecto el usuario
administrador si no hubiera ningun administrador aún.
Para crear un usuario se debe de realizar una petición POST a `/users` con la siguiente
estructura JSON:

|Campo       |Obligatorio |
|------------|------------|
|username    |Sí          |
|firstName   |Sí          |
|surname1    |Sí          |
|surname2    |No          |
|password1   |Sí          |
|password2   |Sí          |
|email       |No          |
|city        |No          |
|phone       |No          |
|profileImage|No          |

Nota: La imagen debe estar codificada en base64.  

Una vez hecho esto puede autentificarse con la aplicación `Eduportal FrontEnd`
de este mismo grupo, obtener el token de acceso desde la consola cuando se listan
los cursos en la web y seguir probando el resto de la API con la autorización 
tipo bearer en postman.